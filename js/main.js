let changeBtn = document.getElementById("btn");
changeBtn.addEventListener("click", changeTheme);
let cssTheme = document.getElementById("css");
let flag;
if (localStorage.getItem("theme") !== null) {
    cssTheme.href = `${localStorage.getItem("theme")}`;
    localStorage.getItem("theme") == "style/main.css" ? flag = 0 : flag = 1;
} else {
    localStorage.setItem("theme", "style/main.css");
    flag = 0;
}

function changeTheme() {
    if (flag == 0) {
        localStorage.setItem("theme", "style/main2.css");
        cssTheme.href = `${localStorage.getItem("theme")}`;
        flag = 1;
    } else {
        localStorage.setItem("theme", "style/main.css");
        cssTheme.href = `${localStorage.getItem("theme")}`;
        flag = 0;
    }
}
